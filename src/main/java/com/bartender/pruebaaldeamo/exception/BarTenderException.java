package com.bartender.pruebaaldeamo.exception;

public class BarTenderException extends Exception {
	
	public BarTenderException(String msg) {
		super(msg);
	}
	
}
