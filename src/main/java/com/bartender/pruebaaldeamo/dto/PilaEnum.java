package com.bartender.pruebaaldeamo.dto;

public enum PilaEnum {
	ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5);

	private int pila;

	PilaEnum(int i) {
		// TODO Auto-generated constructor stub
		this.pila = i;
	}
	
	public int getPila() {
		return this.pila;
	}
}
