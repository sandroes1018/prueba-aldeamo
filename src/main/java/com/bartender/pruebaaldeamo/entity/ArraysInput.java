package com.bartender.pruebaaldeamo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "arrays")
public class ArraysInput {

	@Id
	@Column(name = "id")
	private Integer id;
	
    @NotNull    
    @Column(name = "input_array")
    private String inputArray;
    
    public ArraysInput() {
    	
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInputArray() {
		return inputArray;
	}

	public void setInputArray(String inputArray) {
		this.inputArray = inputArray;
	}
    
}
