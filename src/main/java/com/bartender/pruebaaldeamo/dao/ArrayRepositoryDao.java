package com.bartender.pruebaaldeamo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bartender.pruebaaldeamo.entity.ArraysInput;

@Repository
public interface ArrayRepositoryDao extends JpaRepository<ArraysInput, Integer> {

}
