package com.bartender.pruebaaldeamo.implement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bartender.pruebaaldeamo.dao.ArrayRepositoryDao;
import com.bartender.pruebaaldeamo.entity.ArraysInput;
import com.bartender.pruebaaldeamo.exception.BarTenderException;
import com.bartender.pruebaaldeamo.interfaz.IBartender;
import com.bartender.pruebaaldeamo.utils.Constantes;

@Service
public class BartenderImplement implements IBartender {

	private Logger logger = LoggerFactory.getLogger(BartenderImplement.class);

	@Autowired
	private ArrayRepositoryDao arrayRepositoryDao;

	
	
	private List<Integer> p = Constantes.p;
	private Deque<Integer> a;
	private Deque<Integer> aTmp;
	private Deque<Integer> b;
	private List<Integer> result;

	@Override
	public Object getArray(int iterations, int pila) throws BarTenderException {
		//Inicializar listas
		a = new ArrayDeque<>();
		aTmp = new ArrayDeque<>();
		b = new ArrayDeque<>();
		result = new ArrayList<>();
		//Traer pila guardada en base de datos
		ArraysInput array = arrayRepositoryDao.findById(pila).get();
		this.logger.info(array.getInputArray());
		//Valores de prueba a.add(2); a.add(3); a.add(4); a.add(5); a.add(6); a.add(7);		 
		
		String[] split = array.getInputArray().split(",");
		for (int i = 0; i < split.length; i++) {
			a.add(Integer.parseInt(split[i]));
		}
		//logica re organizar listas
		for (int i = 0; i <= iterations; i++) {		
			this.logger.info("***** P " + p.get(i));
			validateDivisor(p.get(i));		
			this.logger.info("***** B " + b.toString());
			this.logger.info("***** aTMP " + aTmp.toString());
			b.stream().forEach((Integer tmp) -> result.add(tmp));
			b.clear();
			a.clear();
			aTmp.stream().forEach((Integer tmp) -> a.add(tmp));
			aTmp.clear();
			this.logger.info("***** Nuevo VALOR A " + a.toString());
		}
		this.logger.info("******* Respuesta: " + result);
		return result;
	}
	//Logica valor primo
	private void validateDivisor(int divisor) {

		for (Integer integer : a) {
			if (integer % divisor == 0) {
				this.logger.info("******* PRIMO: " + integer);
				b.push(integer);
			} else {
				aTmp.push(integer);
				this.logger.info("******* NO PRIMO: " + integer);
			}
		}

	}

}
