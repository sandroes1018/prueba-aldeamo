package com.bartender.pruebaaldeamo.interfaz;

import org.springframework.stereotype.Service;

import com.bartender.pruebaaldeamo.exception.BarTenderException;

@Service
public interface IBartender {

	Object getArray(int iterations,int pila) throws BarTenderException;
	
}
