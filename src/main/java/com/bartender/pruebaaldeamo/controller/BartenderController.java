package com.bartender.pruebaaldeamo.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bartender.pruebaaldeamo.dto.PilaEnum;
import com.bartender.pruebaaldeamo.exception.BarTenderException;
import com.bartender.pruebaaldeamo.interfaz.IBartender;
import com.bartender.pruebaaldeamo.utils.Constantes;

@RestController
@RequestMapping(Constantes.URL_BASE + "/bartender")
public class BartenderController {

	private Logger logger = LoggerFactory.getLogger(BartenderController.class);

	@Autowired
	private IBartender iBanterder;

	@ResponseBody
	@RequestMapping(value = "get-array/{iterations}/{pila}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getArray(@PathVariable(value = "iterations", required = true) final int iterations,
			@PathVariable(value = "pila", required = true) final PilaEnum pila) {
		this.logger.info("Request getArray() iterations: " + iterations + " / pila: " + pila.getPila());
		this.logger.info("Request getArray() Constantes.p.size(): " + Constantes.p.size());
		try {
			if(iterations >= Constantes.p.size())
				return new ResponseEntity<>("Numero de iteracion superior a lista", HttpStatus.BAD_REQUEST);
			return new ResponseEntity<>(iBanterder.getArray(iterations, pila.getPila()), HttpStatus.OK);
		} catch (BarTenderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
