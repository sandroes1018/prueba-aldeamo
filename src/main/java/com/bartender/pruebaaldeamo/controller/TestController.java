package com.bartender.pruebaaldeamo.controller;

import org.springframework.http.MediaType;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bartender.pruebaaldeamo.utils.Constantes;

@RestController
@RequestMapping(Constantes.URL_BASE + "/test")
public class TestController {

	private Logger logger = LoggerFactory.getLogger(TestController.class);
	//Operacion anonima para validar estado del servicio
	@ResponseBody
	@RequestMapping(value = "echo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> echo() {
		this.logger.info("Request echo() successful");
		Map<Object, Object> response = Map.of("test", "successful");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
